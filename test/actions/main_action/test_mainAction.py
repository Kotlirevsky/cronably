from cronably.actions.main_action.main_action import MainAction
from cronably.actions.post_action.post_action import PostAction
from cronably.context.context import Context
from cronably.model.job import Job
from cronably.utils.cronably_exception import CronablyException

from test.my_test_case import MyTestCase


class TestMainAction(MyTestCase):

    def setUp(self):
        super(TestMainAction, self).setUp()
        self.job_repo = self.db_manager.get_job_repository()
        self.post_action = PostAction({})
        self.context = Context(self.post_action)

    def print_hola_mundo(self):
        print 'hola Mundo'

    def print_hola_mundo_with_fail(self):
        print 'hola Mundo'
        raise CronablyException('Something fail!')

    def test_when_i_run_and_something_fails_It_should_save_out_process_in_post_action(self):
        self.job_repo.create(Job(1, "HOLA_MUNDO"))
        job = self.job_repo.find_by_attribute("NAME", "HOLA_MUNDO")
        self.post_action = PostAction({'report': 'Y'})
        action = MainAction(self.print_hola_mundo_with_fail, job, self.post_action)
        action.run()
        last_task = self.context.get_last_task(job.id)
        output = self.post_action.get_output_from_id(last_task.id)
        self.assertEqual(['hola Mundo', 'Something fail!'], output)

    def test_when_i_run_if_not_has_report_It_should_save_out_process_in_post_action(self):
        self.job_repo.create(Job(1, "HOLA_MUNDO"))
        job = self.job_repo.find_by_attribute("NAME", "HOLA_MUNDO")
        action = MainAction(self.print_hola_mundo, job, self.post_action)
        action.run()
        self.assertEqual({}, self.post_action.process_log)

    def test_when_i_run_It_should_save_out_process_in_post_action(self):
        self.job_repo.create(Job(1, "HOLA_MUNDO"))
        job = self.job_repo.find_by_attribute("NAME", "HOLA_MUNDO")
        self.post_action = PostAction({'report':'Y'})
        action = MainAction(self.print_hola_mundo, job, self.post_action)
        action.run()
        last_task = self.context.get_last_task(job.id)
        output = self.post_action.get_output_from_id(last_task.id)
        self.assertEqual(['hola Mundo'], output)

    def test_when_i_run_I_should_create_a_register_from_start_and_end_process(self):
        self.job_repo.create(Job(1, "HOLA_MUNDO"))
        job = self.job_repo.find_by_attribute("NAME", "HOLA_MUNDO")
        action = MainAction((lambda: "holaMundo"), job, PostAction({}))
        action.run()
        last_task = self.context.get_last_task(job.id)
        self.assertEqual(2, last_task.id)
        self.assertEqual(1, last_task.id_job)

    def test_when_i_start_i_save_the_task_row(self):
        self.job_repo.create(Job(1, "HOLA_MUNDO"))
        job = self.job_repo.find_by_attribute("NAME", "HOLA_MUNDO")
        action = MainAction((lambda: "holaMundo"), job, PostAction({}))
        action._persist_start_process()
        last_task = self.context.get_last_task(job.id)
        self.assertEqual(2, last_task.id)
        self.assertEqual(1, last_task.id_job)

    def test_when_i_end_it_update_the_task_row(self):
        self.job_repo.create(Job(1, "HOLA_MUNDO"))
        job = self.job_repo.find_by_attribute("NAME", "HOLA_MUNDO")
        action = MainAction((lambda: "holaMundo"), job, PostAction({}))
        id_process = action._persist_start_process()
        action._persist_end_process(id_process )
        last_task = self.context.get_last_task(job.id)
        self.assertEqual(2, last_task.id)
        self.assertEqual(1, last_task.id_job)
        self.assertEqual(1, last_task.status)
        self.assertFalse(last_task.msg)
        self.assertTrue(last_task.end)

