from datetime import datetime

from cronably.actions.main_action.main_action import MainAction
from cronably.actions.post_action.post_action import PostAction
from cronably.actions.repetition.weekly_repetition import WeeklyRepetition
from cronably.model.job import Job
from test.my_test_case import MyTestCase


class TestWeeklyRepetition(MyTestCase):

    def test_run_action_second_time(self):
        first_run= self.run_first_time(datetime(2018, 11, 12, 15, 04, 03))
        self.assertFalse(first_run[0])
        first_run[1].__dict__['get_now'] = (lambda: self.mocked_get_now(datetime(2018, 11, 19, 10, 0, 1)))
        second_round = first_run[1].run_action(first_run[2])
        self.assertTrue(second_round)
        self.assertTrue('2018/11/256T10:00:00',first_run[1].next_time_run.strftime("%Y/%m/%dT%H:%M:%S"))

    def test_run_action_first_time(self):
        result= self.run_first_time(datetime(2018, 11, 12, 15, 04, 03))
        was_runned = result[0]
        self.assertFalse(was_runned)
        self.assertTrue('2018/11/19T10:00:00', result[1].next_time_run.strftime("%Y/%m/%dT%H:%M:%S"))

    def run_first_time(self, date_for_now):
        repetition = WeeklyRepetition('MONDAY', '10:00')
        repetition.__dict__['get_now'] = (lambda: self.mocked_get_now(date_for_now))
        repetition.update_next_time_run()
        job = self.create_job()
        main_action = MainAction((lambda: "hola Mundo"), job, PostAction({}))
        result = repetition.run_action(main_action)
        return result, repetition, main_action

    def create_job(self):
        repo = self.db_manager.get_job_repository()
        job = Job(1, 'prueba')
        repo.create(job)
        return job

    def test_update_next_time_run_today_01(self):
        repetition = WeeklyRepetition('MONDAY', '10:00')
        self.evaluate(repetition, datetime(2018, 11, 12, 15, 04, 03), '2018/11/19T10:00:00')

    def test_update_next_time_runtoday_02(self):
        repetition = WeeklyRepetition('MONDAY', '10:00')
        self.evaluate(repetition, datetime(2018, 11, 12, 9, 59, 59), '2018/11/12T10:00:00')

    def test_update_next_time_runtoday_03(self):
        repetition = WeeklyRepetition('MONDAY', '10:00')
        self.evaluate(repetition, datetime(2018, 11, 12, 10, 0, 1), '2018/11/19T10:00:00')

    def test_update_next_time_runtoday_04(self):
        repetition = WeeklyRepetition('MONDAY', '10:00')
        self.evaluate(repetition, datetime(2018, 11, 12, 10, 0, 0), '2018/11/19T10:00:00')

    def test_update_next_time_has_to_run_in_1_day_01(self):
        repetition = WeeklyRepetition('MONDAY', '10:00')
        self.evaluate(repetition, datetime(2018, 11, 11, 10, 0, 1), '2018/11/12T10:00:00')

    def test_update_next_time_has_to_run_in_1_day_02(self):
        repetition = WeeklyRepetition('MONDAY', '10:00')
        self.evaluate(repetition, datetime(2018, 11, 11, 9, 59, 59), '2018/11/12T10:00:00')

    def test_update_next_time_has_to_run_in_2_day_01(self):
        repetition = WeeklyRepetition('MONDAY', '10:00')
        self.evaluate(repetition, datetime(2018, 11, 10, 10, 0, 1), '2018/11/12T10:00:00')

    def test_update_next_time_has_to_run_in_2_day_02(self):
        repetition = WeeklyRepetition('MONDAY', '10:00')
        self.evaluate(repetition, datetime(2018, 11, 10, 9, 59, 59), '2018/11/12T10:00:00')

    def test_update_next_time_has_to_run_in_3_day_01(self):
        repetition = WeeklyRepetition('MONDAY', '10:00')
        self.evaluate(repetition, datetime(2018, 11, 9, 10, 0, 1), '2018/11/12T10:00:00')

    def test_update_next_time_has_to_run_in_3_day_02(self):
        repetition = WeeklyRepetition('MONDAY', '10:00')
        self.evaluate(repetition, datetime(2018, 11, 9, 9, 59, 59), '2018/11/12T10:00:00')

    def test_update_next_time_has_to_run_in_4_day_01(self):
        repetition = WeeklyRepetition('MONDAY', '10:00')
        self.evaluate(repetition, datetime(2018, 11, 8, 10, 0, 1), '2018/11/12T10:00:00')

    def test_update_next_time_has_to_run_in_4_day_02(self):
        repetition = WeeklyRepetition('MONDAY', '10:00')
        self.evaluate(repetition, datetime(2018, 11, 8, 9, 59, 59), '2018/11/12T10:00:00')

    def test_update_next_time_has_to_run_in_5_day_01(self):
        repetition = WeeklyRepetition('MONDAY', '10:00')
        self.evaluate(repetition, datetime(2018, 11, 7, 10, 0, 1), '2018/11/12T10:00:00')

    def test_update_next_time_has_to_run_in_5_day_02(self):
        repetition = WeeklyRepetition('MONDAY', '10:00')
        self.evaluate(repetition, datetime(2018, 11, 7, 9, 59, 59), '2018/11/12T10:00:00')

    def test_update_next_time_has_to_run_in_6_day_01(self):
        repetition = WeeklyRepetition('MONDAY', '10:00')
        self.evaluate(repetition, datetime(2018, 11, 6, 10, 0, 1), '2018/11/12T10:00:00')

    def test_update_next_time_has_to_run_in_6_day_02(self):
        repetition = WeeklyRepetition('MONDAY', '10:00')
        self.evaluate(repetition, datetime(2018, 11, 6, 9, 59, 59), '2018/11/12T10:00:00')

    def test_update_next_time_has_to_run_in_7_day_01(self):
        repetition = WeeklyRepetition('MONDAY', '10:00')
        self.evaluate(repetition, datetime(2018, 11, 5, 10, 0, 1), '2018/11/12T10:00:00')

    def evaluate(self, repetition, date_now, expected):
        repetition.__dict__['get_now'] = (lambda: self.mocked_get_now(date_now))
        repetition.update_next_time_run()
        self.assertEqual(expected, repetition.next_time_run.strftime("%Y/%m/%dT%H:%M:%S"))
