from datetime import datetime

from cronably.actions.repetition.daily_repetition import DailyRepetition
from test.my_test_case import MyTestCase


class TestDailyRepetition(MyTestCase):

    def test_when_is_created_and_first_time_now_before_configuration_and_one_config(self):
        frame_repetition = '10:00'
        result= self.create_repetition(datetime(2018, 11, 12, 9, 59, 59), frame_repetition)
        self.assertTrue('2018/11/11T10:00:00', result.last_time_run.strftime("%Y/%m/%dT%H:%M:%S"))
        self.assertTrue('2018/11/12T10:00:00', result.next_time_run.strftime("%Y/%m/%dT%H:%M:%S"))

    def test_when_is_created_and_first_time_now_equal_configuration_and_one_config(self):
        frame_repetition = '10:00'
        result= self.create_repetition(datetime(2018, 11, 12, 10, 0, 0), frame_repetition)
        self.assertTrue('2018/11/11T10:00:00', result.last_time_run.strftime("%Y/%m/%dT%H:%M:%S"))
        self.assertTrue('2018/11/13T10:00:00', result.next_time_run.strftime("%Y/%m/%dT%H:%M:%S"))

    def test_when_is_created_and_first_time_now_over_configuration_and_one_config(self):
        frame_repetition = '10:00'
        result= self.create_repetition(datetime(2018, 11, 12, 10, 0, 1), frame_repetition)
        self.assertTrue('2018/11/11T10:00:00', result.last_time_run.strftime("%Y/%m/%dT%H:%M:%S"))
        self.assertTrue('2018/11/13T10:00:00', result.next_time_run.strftime("%Y/%m/%dT%H:%M:%S"))

    # ----------------------
    # two values for config

    def test_when_is_created_and_first_time_now_before_configuration_and_two_config(self):
        frame_repetition = '10:00, 14:00'
        result= self.create_repetition(datetime(2018, 11, 12, 9, 59, 59), frame_repetition)
        self.assertTrue('2018/11/11T10:00:00', result.last_time_run.strftime("%Y/%m/%dT%H:%M:%S"))
        self.assertTrue('2018/11/12T10:00:00', result.next_time_run.strftime("%Y/%m/%dT%H:%M:%S"))

    def test_when_is_created_and_now_is_first_config(self):
        frame_repetition = '10:00, 14:00'
        result= self.create_repetition(datetime(2018, 11, 12, 10, 0, 0), frame_repetition)
        self.assertTrue('2018/11/11T10:00:00', result.last_time_run.strftime("%Y/%m/%dT%H:%M:%S"))
        self.assertTrue('2018/11/12T14:00:00', result.next_time_run.strftime("%Y/%m/%dT%H:%M:%S"))

    def test_when_is_created_and_first_time_now_between_first_and_second_config(self):
        frame_repetition = '10:00, 14:00'
        result= self.create_repetition(datetime(2018, 11, 12, 10, 0, 1), frame_repetition)
        self.assertTrue('2018/11/11T10:00:00', result.last_time_run.strftime("%Y/%m/%dT%H:%M:%S"))
        self.assertTrue('2018/11/12T14:00:00', result.next_time_run.strftime("%Y/%m/%dT%H:%M:%S"))

    def test_when_is_created_and_now_is_second_config(self):
        frame_repetition = '10:00, 14:00'
        result= self.create_repetition(datetime(2018, 11, 12, 14, 0, 0), frame_repetition)
        self.assertTrue('2018/11/11T10:00:00', result.last_time_run.strftime("%Y/%m/%dT%H:%M:%S"))
        self.assertTrue('2018/11/13T10:00:00', result.next_time_run.strftime("%Y/%m/%dT%H:%M:%S"))

    def test_when_is_created_and_now_is_over_second_config(self):
        frame_repetition = '10:00, 14:00'
        result= self.create_repetition(datetime(2018, 11, 12, 14, 0, 1), frame_repetition)
        self.assertTrue('2018/11/11T10:00:00', result.last_time_run.strftime("%Y/%m/%dT%H:%M:%S"))
        self.assertTrue('2018/11/13T10:00:00', result.next_time_run.strftime("%Y/%m/%dT%H:%M:%S"))

    # ----------------------
    # three values for config

    def test_when_you_have_three_configs_and_is_created_and_first_time_now_before_configuration(self):
        frame_repetition = '10:00, 14:00, 20:00'
        result= self.create_repetition(datetime(2018, 11, 12, 9, 59, 59), frame_repetition)
        self.assertTrue('2018/11/11T10:00:00', result.last_time_run.strftime("%Y/%m/%dT%H:%M:%S"))
        self.assertTrue('2018/11/12T10:00:00', result.next_time_run.strftime("%Y/%m/%dT%H:%M:%S"))

    def test_when_you_have_three_configs_and_now_is_first_config(self):
        frame_repetition = '10:00, 14:00, 20:00'
        result= self.create_repetition(datetime(2018, 11, 12, 10, 0, 0), frame_repetition)
        self.assertTrue('2018/11/11T10:00:00', result.last_time_run.strftime("%Y/%m/%dT%H:%M:%S"))
        self.assertTrue('2018/11/12T14:00:00', result.next_time_run.strftime("%Y/%m/%dT%H:%M:%S"))

    def test_when_you_have_three_configs_and_first_time_now_between_first_and_second_config(self):
        frame_repetition = '10:00, 14:00, 20:00'
        result= self.create_repetition(datetime(2018, 11, 12, 10, 0, 1), frame_repetition)
        self.assertTrue('2018/11/11T10:00:00', result.last_time_run.strftime("%Y/%m/%dT%H:%M:%S"))
        self.assertTrue('2018/11/12T14:00:00', result.next_time_run.strftime("%Y/%m/%dT%H:%M:%S"))

    def test_when_you_have_three_configs_and_now_is_second_config(self):
        frame_repetition = '10:00, 14:00, 20:00'
        result= self.create_repetition(datetime(2018, 11, 12, 14, 0, 0), frame_repetition)
        self.assertTrue('2018/11/11T10:00:00', result.last_time_run.strftime("%Y/%m/%dT%H:%M:%S"))
        self.assertTrue('2018/11/12T20:00:00', result.next_time_run.strftime("%Y/%m/%dT%H:%M:%S"))

    def test_when_you_have_three_configs_and_first_time_now_between_second__and_thirdconfig(self):
        frame_repetition = '10:00, 14:00, 20:00'
        result= self.create_repetition(datetime(2018, 11, 12, 14, 0, 1), frame_repetition)
        self.assertTrue('2018/11/11T10:00:00', result.last_time_run.strftime("%Y/%m/%dT%H:%M:%S"))
        self.assertTrue('2018/11/12T20:00:00', result.next_time_run.strftime("%Y/%m/%dT%H:%M:%S"))

    def test_when_you_have_three_configs_and_now_is_third_config(self):
        frame_repetition = '10:00, 14:00, 20:00'
        result= self.create_repetition(datetime(2018, 11, 12, 20, 0, 0), frame_repetition)
        self.assertTrue('2018/11/11T10:00:00', result.last_time_run.strftime("%Y/%m/%dT%H:%M:%S"))
        self.assertTrue('2018/11/13T10:00:00', result.next_time_run.strftime("%Y/%m/%dT%H:%M:%S"))

    def test_when_you_have_three_configs_and_now_is_over_third_config(self):
        frame_repetition = '10:00, 14:00, 20:00'
        result= self.create_repetition(datetime(2018, 11, 12, 20, 0, 1), frame_repetition)
        self.assertTrue('2018/11/11T10:00:00', result.last_time_run.strftime("%Y/%m/%dT%H:%M:%S"))
        self.assertTrue('2018/11/13T10:00:00', result.next_time_run.strftime("%Y/%m/%dT%H:%M:%S"))

    def create_repetition(self, date_for_now, frame_repetition):
        repetition = DailyRepetition(frame_repetition)
        repetition.__dict__['get_now'] = (lambda: self.mocked_get_now(date_for_now))
        repetition.initialize_last_time()
        repetition.update_next_time_run()
        return repetition
