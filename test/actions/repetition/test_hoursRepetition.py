from datetime import timedelta, datetime

from cronably.actions.main_action.main_action import MainAction
from cronably.actions.post_action.post_action import PostAction
from cronably.actions.repetition.std_repetition import StdRepetition
from cronably.model.job import Job
from test.my_test_case import MyTestCase


class TestHoursRepetition(MyTestCase):

    def test_run_action_second_time(self):
        repetition, main_action = self.run_first_time()
        repetition.__dict__['get_now'] = (lambda:self.mocked_get_now())
        last_time_before = repetition.last_time_run
        next_time_before = repetition.next_time_run
        repetition.run_action(main_action)
        self.assertTrue(repetition.next_time_run - repetition.last_time_run > timedelta(minutes=120))
        self.assertTrue(repetition.next_time_run - repetition.last_time_run < timedelta(minutes=121))
        self.assertTrue(repetition.last_time_run == next_time_before)
        self.assertTrue(repetition.last_time_run - last_time_before > timedelta(minutes=120))
        self.assertTrue(repetition.next_time_run - next_time_before > timedelta(minutes=120))

    def test_run_action_first_time(self):
        repetition= self.run_first_time()[0]
        self.assertTrue(repetition.next_time_run - repetition.last_time_run > timedelta(minutes=120))
        self.assertTrue(repetition.next_time_run - repetition.last_time_run < timedelta(minutes=121))

    def run_first_time(self):
        repetition = StdRepetition('HOURS', 2)
        job = self.create_job()
        main_action = MainAction((lambda: "hola Mundo"), job, PostAction({}))
        repetition.run_action(main_action)
        return repetition, main_action

    def create_job(self):
        repo = self.db_manager.get_job_repository()
        job = Job(1, 'prueba')
        repo.create(job)
        return job

    def mocked_get_now(self):
        return datetime.now() + timedelta(minutes=120)

    def tearDown(self):
        self.times_called = 1
        super(TestHoursRepetition,self).tearDown()