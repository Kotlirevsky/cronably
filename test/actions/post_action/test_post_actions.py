import os

from cronably.actions.post_action.post_action import PostAction
from cronably.actions.pre_action.pre_actions import PreActions
from cronably.context.context import Context
from test.my_test_case import MyTestCase


class TestPostAction(MyTestCase):

    def setUp(self):
        super(TestPostAction, self).setUp()

    def test_after_all_actions_run_should_take_db_task_and_create_report(self):
        # given:
        self.preset_conf_file()
        actions, context, main_action = self.prepare_context()
        context.execute(main_action, actions.repetition)
        # when
        PostAction(actions.processed_parameters).run()
        # then
        self.assertTrue(os.path.exists('./cronably_report.txt'))

    def prepare_context(self):
        conf = {'ext_config': True}
        main_action = (lambda: 'hola mundo')
        actions = PreActions(conf)
        post_action = PostAction(actions.processed_parameters)
        context = Context(post_action)
        return actions, context, main_action

    def preset_conf_file(self):
        values = {'name': 'HOLA_MUNDO', 'repetition': {'frame': 'SECONDS', 'period': 1}, 'loops': 2, 'report': 'Y'}
        self.setup_conf_file(values)

    def tearDown(self):
        super(TestPostAction, self).tearDown()
        self.remove("./cronably_report.txt")
        self.remove("./cronably.txt")
