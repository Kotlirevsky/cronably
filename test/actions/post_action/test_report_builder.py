import os

from cronably.actions.post_action.report_builder import ReportBuilder
from cronably.model.task import Task

from cronably.model.job import Job
from test.my_test_case import MyTestCase


class TestReportBuilder(MyTestCase):


    def setUp(self):
        super(TestReportBuilder, self).setUp()

    def test_should_create_report(self):
        self.create_job_with_tasks()
        reporter = self.create_reporter()
        reporter.build()
        self.assertTrue(os.path.exists("./cronably_report.txt"))

    def tearDown(self):
        super(TestReportBuilder, self).tearDown()
        self.remove("./cronably_report.txt")

    def create_job_with_tasks(self):
        job = Job(1, 'A_JOB' )
        task_01 = Task(1, 1, 'start_01', 'end_01', 1)
        task_02 = Task(2, 1, 'start_02', 'end_02', 1)
        self.db_manager.get_job_repository().create(job)
        id= self.db_manager.get_task_repository().create(task_01)
        self.db_manager.get_task_repository().update_end(id, 1, 'OK')
        id =self.db_manager.get_task_repository().create(task_02)
        self.db_manager.get_task_repository().update_end(id, 1,'OK')

    def create_reporter(self):
        process_log ={1:['some_report_0101', 'some_report_0102', 'some_report_0103'], 2:['some_report_0201', 'some_report_0202', 'some_report_0203']}
        params = {'db_kind': ':memory:', 'repetition.frame': 'seconds', 'repetition.period': '1', 'ext_config': True, 'name': 'A_JOB', 'report':'Y'}
        reporter = ReportBuilder(params, process_log)
        return reporter

