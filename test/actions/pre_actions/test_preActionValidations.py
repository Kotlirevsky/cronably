from cronably.actions.pre_action.pre_action_validations import PreActionValidations
from cronably.utils.cronably_exception import CronablyException
from test.my_test_case import MyTestCase


class TestPreActionValidations(MyTestCase):

    def test_has_repetition_strategy_from_file(self):
        parameters ={'ext_config':True, 'repetition.frame':'MINUTES', 'repetition.period':1}
        validator = PreActionValidations(parameters)
        self.assertTrue(validator.has_repetition_strategy())

    def test_has_repetition_weekly_strategy_from_file(self):
        parameters ={'ext_config':True, 'repetition.frame':'WEEKLY', 'repetition.period.day':'MONDAY',
                     'repetition.period.time':'10:00'}
        validator = PreActionValidations(parameters)
        self.assertTrue(validator.has_repetition_strategy())

    def test_has_repetition_weekly_strategy_from_file_miss_param_day_raise_ex(self):
        parameters ={'ext_config':True, 'repetition.frame':'WEEKLY','repetition.period.time':'10:00'}
        validator = PreActionValidations(parameters)
        self.evaluate_for_fail(validator, 'repetition.period.day', 'repetition.period.time')

    def test_has_repetition_weekly_strategy_from_file_miss_param_time_raise_ex(self):
        parameters ={'ext_config':True, 'repetition.frame':'WEEKLY','repetition.period.day':'MONDAY'}
        validator = PreActionValidations(parameters)
        self.evaluate_for_fail(validator, 'repetition.period.day', 'repetition.period.time')

    def test_has_repetition_weekly_strategy_from_code_miss_param_day_raise_ex(self):
        parameters ={'repetition_frame':'WEEKLY', 'repetition_period_time':'10:00'}
        validator = PreActionValidations(parameters)
        self.evaluate_for_fail(validator, 'repetition_period_day', 'repetition_period_time')

    def test_has_repetition_weekly_strategy_from_code_miss_param_time_raise_ex(self):
        parameters ={'repetition_frame':'WEEKLY', 'repetition_period_day':'MONDAY'}
        validator = PreActionValidations(parameters)
        self.evaluate_for_fail(validator, 'repetition_period_day', 'repetition_period_time')

    def test_has_repetition_weekly_strategy_from_code(self):
        parameters ={'repetition_frame':'WEEKLY', 'repetition_period_day':'MONDAY',
                     'repetition_period_time':'10:00'}
        validator = PreActionValidations(parameters)
        self.assertTrue(validator.has_repetition_strategy())

    def test_has_repetition_strategy_from_code(self):
        parameters ={'repetetion_frame':'MINUTES','repetition_frame':'MINUTES', 'repetition_period':1}
        validator = PreActionValidations(parameters)
        self.assertTrue(validator.has_repetition_strategy())

    def test_has_repetition_strategy_from_file_with_no_frame(self):
        parameters ={'ext_config':True , 'repetition.period':1}
        validator = PreActionValidations(parameters)
        self.evaluate_for_fail(validator, 'frame','period')

    def test_has_repetition_strategy_from_code_with_no_frame(self):
        parameters ={'repetition_period':1}
        validator = PreActionValidations(parameters)
        self.evaluate_for_fail(validator, 'frame','period')

    def test_when_no_db_is_configured_it_use_memory_db(self):
        parameters ={}
        validator = PreActionValidations(parameters)
        validator.check_db_kind()
        self.assertEqual(':memory:',validator.parameters['db_kind'])

    def test_when_db_is_configured_as_memory(self):
        parameters ={'db_kind':'memory'}
        validator = PreActionValidations(parameters)
        validator.check_db_kind()
        self.assertEqual(':memory:',validator.parameters['db_kind'])

    def test_when_db_is_configured_configured_with_a_value(self):
        parameters ={'db_kind':'zarlanga'}
        validator = PreActionValidations(parameters)
        validator.check_db_kind()
        self.assertEqual('zarlanga',validator.parameters['db_kind'])

    def evaluate_for_fail(self, validator, key1, key2):
        try:
            validator.has_repetition_strategy()
            self.fail()
        except CronablyException as ce:
            self.assertEqual('Repetition miss params, check you add %s and %s' % (key1, key2), ce.args[0])
