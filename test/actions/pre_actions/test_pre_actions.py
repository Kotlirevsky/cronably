import os

from cronably.actions.pre_action.pre_actions import PreActions
from cronably.actions.repetition.daily_repetition import DailyRepetition
from cronably.actions.repetition.weekly_repetition import WeeklyRepetition
from cronably.repositories.db_manager import DbManager
from cronably.utils.cronably_exception import CronablyException
from test.my_test_case import MyTestCase


class TestPreActions(MyTestCase):

    def setUp(self):
        DbManager.configure_db()
        self.job_repo = DbManager.get_instance().get_job_repository()

    def test_if_has_repetition_daily_way_should_contains_repetition(self):
        values = {'name': 'HOLA_MUNDO', 'repetition': {'frame': 'DAILY', 'period': '10:00,14:00'}}
        self.setup_conf_file(values)
        params = {"ext_config": True}
        actions = PreActions(params)
        self.assertTrue(isinstance(actions.repetition, DailyRepetition))

    def test_if_has_repetition_by_day_name_and_time_should_contains_repetition(self):
        values = {'name': 'HOLA_MUNDO', 'repetition': {'frame': 'WEEKLY', 'period': {'day':'MONDAY','time':'10:00'}}}
        self.setup_conf_file(values)
        params = {"ext_config": True}
        actions = PreActions(params)
        self.assertTrue(isinstance(actions.repetition, WeeklyRepetition))

    def test_if_has_repetition_by_days_and_quantity_should_contains_repetition(self):
        values = {'name': 'HOLA_MUNDO', 'repetition': {'frame': 'DAYS', 'period': 2}}
        self.setup_conf_file(values)
        params = {"ext_config": True}
        actions = PreActions(params)
        self.assertTrue(actions.repetition.kind == 'DAYS')

    def test_if_has_repetition_by_minutes_and_quantity_should_contains_repetition(self):
        values = {'name': 'HOLA_MUNDO', 'repetition': {'frame': 'MINUTES', 'period': 1}}
        self.setup_conf_file(values)
        params = {"ext_config": True}
        actions = PreActions(params)
        self.assertTrue(actions.repetition.kind == 'MINUTES')

    def test_if_has_repetition_by_seconds_and_quantity_should_contains_repetition(self):
        values = {'name': 'HOLA_MUNDO', 'repetition': {'frame': 'SECONDS', 'period': 1}}
        self.setup_conf_file(values)
        params = {"ext_config": True}
        actions = PreActions(params)
        self.assertTrue(actions.repetition.kind == 'SECONDS')

    def test_if_has_repetition_by_hours_and_quantity_should_contains_repetition(self):
        values = {'name': 'HOLA_MUNDO', 'repetition': {'frame': 'HOURS', 'period': 1}}
        self.setup_conf_file(values)
        params = {"ext_config": True}
        actions = PreActions(params)
        self.assertTrue(actions.repetition.kind == 'HOURS')

    def test_if_has_repetition_by_and_frame_is_not_setted_should_raise_exc(self):
        values = {'name': 'HOLA_MUNDO', 'repetition': {'period': 1}}
        try:
            self.setup_conf_file(values)
            params = {"ext_config": True}
            PreActions(params)
            self.fail()
        except CronablyException as ce:
            self.assertEqual('Repetition miss params, check you add frame and period', str(ce))

    def test_if_has_repetition_by_minutes_should_and_not_quantity_should_raise_exc(self):
        values = {'name': 'HOLA_MUNDO', 'repetition': {'frame': 'MINUTES'}}
        try:
            self.setup_conf_file(values)
            params = {"ext_config": True}
            PreActions(params)
            self.fail()
        except CronablyException as ce:
            self.assertEqual('Repetition miss params, check you add frame and period', str(ce))

    def test_when_run_with_params_name_should_create_configuration(self):
        params = {"name":"CARLOS", "loops": 1}
        PreActions(params)
        self.assertTrue(self.job_repo.find_by_attribute('NAME',"CARLOS"))

    def test_when_run_without_params_name_should_raise_ex(self):
        try:
            params = {}
            PreActions(params)
            self.fail()
        except CronablyException as ce:
            self.assertEqual('You should add a name into your Cronably annotation', str(ce))

    def test_when_run_without_params_loop_should_save_it_with_1(self):
        params = {"name":"CARLOS"}
        PreActions(params)
        job = self.job_repo.find_by_attribute("NAME", "CARLOS")
        self.assertEqual(1, job.loops)

    def test_when_run_with_params_loop_equals10_should_save_it(self):
        params = {"name": "CARLOS", "loops":10}
        PreActions(params)
        job = self.job_repo.find_by_attribute("NAME", "CARLOS")
        self.assertEqual(10, job.loops)

    def test_when_run_with_no_report_flag_report_should_be_saved_as_false(self):
        params = {"name": "CARLOS"}
        PreActions(params)
        job = self.job_repo.find_by_attribute("NAME", "CARLOS")
        self.assertFalse(job.report)

    def test_if_is_loaded_with_the_key_with_file_should_load_from_file(self):
        self.setup_conf_file({'name': 'CARLOS'})
        params = {"ext_config": True}
        PreActions(params)
        job = self.job_repo.find_by_attribute("NAME", "CARLOS")
        self.assertFalse(job.report)

    def test_if_is_loaded_with_no_db_kind_parameter_should_be_memory_db(self):
        self.tearDown()
        self.setup_conf_file({'name': 'CARLOS'})
        params = {"ext_config": True}
        actions = PreActions(params)
        self.assertEqual(':memory:', actions.processed_parameters['db_kind'])

    def test_if_is_loaded_with_no_db_kind_parameter_should_be_concrete_db(self):
        self.tearDown()
        self.setup_conf_file({'name': 'CARLOS', 'db_kind':'cronably'})
        params = {"ext_config": True}
        actions = PreActions(params)
        self.assertEqual('cronably', actions.processed_parameters['db_kind'])
        self.assertTrue(os.path.exists('./cronably'))

    def test_when_run_with_report_flag_report_should_be_saved_as_1(self):
        params = {"name": "CARLOS", "report": "Y"}
        PreActions(params)
        job = self.job_repo.find_by_attribute("NAME", "CARLOS")
        self.assertTrue(job.report)

    def tearDown(self):
        super(TestPreActions, self).tearDown()
        self.remove("./cronably.txt")
        self.remove("./cronably")
