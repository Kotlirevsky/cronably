import unittest

from test.actions.post_action.test_post_actions import TestPostAction
from test.actions.post_action.test_report_builder import TestReportBuilder
from test.actions.pre_actions.test_preActionValidations import TestPreActionValidations
from test.actions.pre_actions.test_pre_actions import TestPreActions
from test.actions.repetition.test_dailyRepetition import TestDailyRepetition
from test.actions.repetition.test_daysRepetition import TestDaysRepetition
from test.actions.repetition.test_hoursRepetition import TestHoursRepetition
from test.actions.repetition.test_minutesRepetition import TestMinutesRepetition
from test.actions.repetition.test_secondsRepetition import TestSecondsRepetition
from test.actions.repetition.test_weeklyRepetition import TestWeeklyRepetition
from test.actions.main_action.test_mainAction import TestMainAction
from test.context.test_context import TestContext
from test.test_cronably import TestCronably


def suite():
    suite = unittest.TestSuite()
    suite.addTest(TestMainAction())
    suite.addTest(TestCronably())
    suite.addTest(TestPreActions())
    suite.addTest(TestContext())
    suite.addTest(TestMinutesRepetition())
    suite.addTest(TestHoursRepetition())
    suite.addTest(TestSecondsRepetition())
    suite.addTest(TestDaysRepetition())
    suite.addTest(TestPreActionValidations())
    suite.addTest(TestWeeklyRepetition())
    suite.addTest(TestDailyRepetition())
    suite.addTest(TestPostAction())
    suite.addTest(TestReportBuilder())
    return suite


if __name__ == '__main__':
    runner = unittest.TextTestRunner()
    runner.run(suite())