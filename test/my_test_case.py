import os
import unittest
from unittest import TestCase

from cronably.repositories.db_manager import DbManager


class MyTestCase(TestCase):

    cronably_file="./cronably.txt"

    def setUp(self):
        DbManager.configure_db()
        self.db_manager = DbManager.get_instance()

    def runTest(self):
        suite = unittest.defaultTestLoader.loadTestsFromTestCase(type(self))
        unittest.TextTestRunner().run(suite)

    def tearDown(self):
        DbManager.reboot()

    def remove(self, pathx):
        if os.path.exists(pathx):
            os.remove(pathx)

    def setup_conf_file(self, values):
        with open(self.cronably_file, 'w') as fx:
            for key,val in values.items():
                if type(val) == dict:
                    for key_1, val_1 in val.items():
                        if key_1 == 'period':
                            if type(val_1) == dict:
                                for key_2, val_2 in val_1.items():
                                    fx.write("%s%s%s\n" % ('%s.%s.%s' % (key,key_1,key_2), '=', val_2))
                            else:
                                fx.write("%s%s%s\n" % ('%s.%s' % (key,key_1), '=', val_1))
                        else:
                            fx.write("%s%s%s\n" % ('%s.%s' % (key, key_1), '=', val_1))
                else:
                    fx.write("%s%s%s\n" % (key, '=', val))

    def mocked_get_now(self, date):
        return date


if __name__ == '__main__':
     unittest.main()

