from datetime import timedelta, datetime

from cronably.actions.post_action.post_action import PostAction
from cronably.actions.pre_action.pre_actions import PreActions
from cronably.actions.repetition.std_repetition import StdRepetition
from cronably.context.context import Context
from cronably.model.job import Job
from cronably.repositories.db_manager import DbManager
from test.my_test_case import MyTestCase


class TestContext(MyTestCase):

    def setUp(self):
        super(TestContext, self).setUp()
        self.job_repo = self.db_manager.get_job_repository()


    def test_when_execute_task_with_loops_0_as_increase_not_should_run_again(self):
        context =Context(PostAction({'name':'SOMETHING'}))
        context.__dict__['execute_main_action']=(lambda x,y,z: True)
        self.job_repo.create(Job(1, 'SOMETHING', True, 1))
        action = (lambda : True)
        should_run, loops= context.task_execution(action, 0, StdRepetition('MINUTES', 1))
        self.assertFalse(should_run)

    def test_when_execute_task_with_loops_0_as_increase_and_job_is_stoped_not_should_run_again(self):
        context = Context(PostAction({'name': 'SOMETHING'}))
        context.__dict__['execute_main_action']=(lambda x,y,z: True)
        self.job_repo.create(Job(1, 'SOMETHING', True, 10))
        action = (lambda : True)
        should_run, loops= context.task_execution(action, 0, StdRepetition('MINUTES', 1))
        self.assertTrue(should_run)
        self.job_repo.stop_job(1)
        should_run, loops = context.task_execution(action, loops, StdRepetition('MINUTES', 1))
        self.assertFalse(should_run)
        self.assertEqual(2, loops)

    def test_when_I_create_context_should_create_db_in_memory(self):
        context = Context(PostAction({'name': 'SOMETHING'}))
        self.assertTrue(context.is_db_active())

    def test_when_ask_if_process_exists_and_it_does_not_should_return_True(self):
        context = Context(PostAction({'name': 'SOMETHING'}))
        self.assertFalse(context.check_exist_job("NOT_EXISING_JOB"))

    def test_execute_with_seconds_repetition(self):
        self.evaluate_time_frame('SECONDS', 10, 10,  11)

    def test_execute_with_minute_repetition(self):
        self.evaluate_time_frame('MINUTES', 1, 60,   61)

    def test_execute_with_hours_repetition(self):
        duration_seconds = 2 * 60 * 60
        self.evaluate_time_frame('HOURS', 2, duration_seconds, duration_seconds + 1)

    def test_execute_with_days_repetition(self):
        duration_seconds = 2 * 24 * 60 * 60
        self.evaluate_time_frame('DAYS', 2, duration_seconds, duration_seconds + 1)

    def test_execute_with_weekly(self):
        #given:
        action = (lambda: 'something')
        values = {'name': 'HOLA_MUNDO', 'repetition': {'frame':'WEEKLY', 'period':{'day':'MONDAY', 'time':'10:00'}}}
        self.setup_conf_file(values)
        params = {"ext_config": True}
        pre_action = PreActions(params)
        # Pre cron horario..
        pre_action.repetition.__dict__['get_now'] = (lambda: datetime(2018, 11, 12, 9, 50, 1))
        pre_action.repetition.update_next_time_run()
        pre_action.repetition.__dict__['get_now'] = (lambda: datetime(2018, 11, 12, 10, 0, 1))
        context = Context(PostAction(pre_action.processed_parameters))
        #when:
        context.execute(action, pre_action.repetition)
        #then:
        self.assertTrue('2018/11/19T10:00:00', pre_action.repetition.next_time_run.strftime("%Y/%m/%dT%H:%M:%S"))
        job = self.job_repo.find_by_attribute('NAME', 'HOLA_MUNDO')
        last_task = context.get_last_task(job.id)
        self.assertEqual(2, last_task.id)

    def test_execute_with_daily_first_time(self):
        #given:
        action = (lambda: 'something')
        values = {'name': 'HOLA_MUNDO', 'repetition': {'frame':'DAILY', 'period':'10:00,14:00'}}
        self.setup_conf_file(values)
        params = {"ext_config": True}
        pre_action = PreActions(params)
        # Pre cron horario..
        pre_action.repetition.__dict__['get_now'] = (lambda: datetime(2018, 11, 12, 9, 50, 1))
        pre_action.repetition.update_next_time_run()
        pre_action.repetition.__dict__['get_now'] = (lambda: datetime(2018, 11, 12, 10, 0, 0))
        context = Context(PostAction(pre_action.processed_parameters))
        #when:
        context.execute(action, pre_action.repetition)
        #then:
        self.assertTrue('2018/11/12T14:00:00', pre_action.repetition.next_time_run.strftime("%Y/%m/%dT%H:%M:%S"))
        job = self.job_repo.find_by_attribute('NAME', 'HOLA_MUNDO')
        last_task = context.get_last_task(job.id)
        self.assertEqual(2, last_task.id)

    def test_execute_with_daily_second_time(self):
        #given:
        action = (lambda: 'something')
        values = {'name': 'HOLA_MUNDO', 'repetition': {'frame':'DAILY', 'period':'10:00,14:00'}}
        self.setup_conf_file(values)
        params = {"ext_config": True}
        pre_action = PreActions(params)
        # Pre cron horario..
        pre_action.repetition.__dict__['get_now'] = (lambda: datetime(2018, 11, 12, 9, 50, 1))
        pre_action.repetition.update_next_time_run()
        pre_action.repetition.__dict__['get_now'] = (lambda: datetime(2018, 11, 12, 10, 0, 0))
        context = Context(PostAction(pre_action.processed_parameters))
        context.execute(action, pre_action.repetition)
        pre_action.repetition.__dict__['get_now'] = (lambda: datetime(2018, 11, 12, 14, 0, 0))
        context.execute(action, pre_action.repetition)
        #then:
        self.assertTrue('2018/11/13T10:00:00', pre_action.repetition.next_time_run.strftime("%Y/%m/%dT%H:%M:%S"))
        job = self.job_repo.find_by_attribute('NAME', 'HOLA_MUNDO')
        last_task = context.get_last_task(job.id)
        self.assertEqual(2, last_task.id)

    def create_job(self):
        repo = self.db_manager.get_job_repository()
        job = Job(1, 'prueba')
        repo.create(job)
        return job

    def create_parameters(self, frame, period):
        values = {'name': 'HOLA_MUNDO', 'repetition': {'frame': frame, 'period': period}}
        self.setup_conf_file(values)
        params = {"ext_config": True}
        return PreActions(params).processed_parameters

    def tearDown(self):
        DbManager.get_instance().reboot()
        self.remove("./cronably.txt")

    def evaluate_time_frame(self, frame, duration, duration_seconds, max_duration):
        action = (lambda: 'something')
        repetition = StdRepetition(frame, duration)
        parameters = self.create_parameters(frame, duration)
        context = Context(PostAction(parameters))
        context.execute(action, repetition)
        differences = repetition.next_time_run - repetition.last_time_run
        self.assertTrue(differences > timedelta(seconds=duration_seconds))
        self.assertTrue(differences < timedelta(seconds=max_duration))
        job = self.job_repo.find_by_attribute('NAME', 'HOLA_MUNDO')
        last_task = context.get_last_task(job.id)
        self.assertEqual(2, last_task.id)
        self.assertEqual(1, last_task.id_job)