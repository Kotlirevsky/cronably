#!/usr/bin/env bash

CRONABLY="/Users/davidgk/development/personal/python/cronably"
PRUEBA="/Users/davidgk/Desktop/porquerias/personal/example_cronably/prueba"


cd $CRONABLY

rm -rf dist/*

python setup.py sdist bdist_wheel

cd $PRUEBA

rm cronably-*.tar.gz
rm Pipfile*

pipenv install

cp $CRONABLY/dist/cronably-1.3.1.tar.gz $PRUEBA/.

pipenv install $PRUEBA/cronably-1.3.1.tar.gz

python prueba.py


# twine upload dist/*