from cronably.cronably_main import Cronably


@Cronably(name='PROCESS_01', repetition_frame='MINUTES' , repetition_period=1, loops= 2 )
def my_process_with_repetitions_minutes():
    print "Hola Mundo"


if __name__ == '__main__':
    my_process_with_repetitions_minutes()