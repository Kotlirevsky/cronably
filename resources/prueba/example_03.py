from cronably.cronably_main import Cronably


@Cronably(ext_config=True)
def my_process_from_file():
    print "Hola Mundo"


if __name__ == '__main__':
    my_process_from_file()